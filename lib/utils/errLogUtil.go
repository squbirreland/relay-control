package utils

import "log"

func TryThrow(e error) {
	if e != nil {
		log.Fatal(e.Error())
	}
}
