package utils

import (
	"io"
	"log"
	"relay-control/lib/serial"
	"relay-control/src/basic/base"
)

func OpenSerialPort(options serial.OpenOptions) io.ReadWriteCloser {
	if serial.IsStandardBaudRate(options.BaudRate) {
		open, err := serial.Open(options)
		TryThrow(err)
		return open
	} else {
		log.Fatal(" 配置中有错误属性 : 非常规的比特率")
	}
	return nil
}

func FindPortRw(portName string) io.ReadWriteCloser {
	for k, v := range base.SerialPortMap {
		if k == portName {
			return v
		}
	}
	return nil
}
