package properties

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"relay-control/lib/serial"
	"relay-control/lib/utils"
	"relay-control/src/basic"
)

var Properties JsonProperties

type JsonProperties struct {
	ReadSensitivity int64                `json:"ReadSensitivity"`
	PortOptions     []serial.OpenOptions `json:"PortOptions"`
}

func ReadConfigJson() {
	//获取文件路径
	wd, err := os.Getwd()
	utils.TryThrow(err)
	filePath := wd + "/" + basic.ConfigFile
	//读取文件
	file, err := ioutil.ReadFile(filePath)
	utils.TryThrow(err)
	//转换
	properties := JsonProperties{}
	err = json.Unmarshal(file, &properties)
	utils.TryThrow(err)
	Properties = properties
}
