package mvc

type Req struct {
	Method string
	Addr   string
}

func NewReq(method string, addr string) *Req {
	return &Req{Method: method, Addr: addr}
}
