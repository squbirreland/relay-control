package listener

import (
	"io"
	"log"
	"relay-control/lib/utils"
	"relay-control/src/basic/base"
	"relay-control/src/machine/properties"
	"time"
)

//ListenPort 触发监听入口
func ListenPort(portName string) {
	portRW := utils.FindPortRw(portName)
	if portRW != nil {
		defer func(portRW io.ReadWriteCloser) {
			err := portRW.Close()
			if err != nil {
				log.Fatal(" port close failed ")
			}
		}(portRW)
		for {
			//由于灵敏度过高 进行读取延迟 两次读取 并将结果合并
			first := read(portRW, 4, 8)
			time.Sleep(time.Duration(properties.Properties.ReadSensitivity))
			delay := read(portRW, 8, 16)
			result := append(first, delay...)
			base.Logger.Info().Printf(" [%s] >> %X \n", portName, result)
			//todo 对结果的逻辑处理

			time.Sleep(time.Duration(properties.Properties.ReadSensitivity * 2))
		}
	}
}

func read(portRW io.ReadWriteCloser, byteLen int, byteCap int) []byte {
	bytes := make([]byte, byteLen, byteCap)
	read, err := portRW.Read(bytes)
	if err != nil {
		log.Println(" port read failed ")
	}
	return bytes[:read]
}
