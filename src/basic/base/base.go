package base

import (
	"io"
	"relay-control/lib/serial"
	"relay-control/lib/utils/logger"
)

var SerialPortMap = make(map[string]io.ReadWriteCloser, 0)
var Options = make([]serial.OpenOptions, 0, 4)
var Logger = logger.NewLoggerFactory(false, "").GetLogger()
