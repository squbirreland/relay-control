package web

import (
	"net/http"
	"relay-control/src/basic/base"
)

func inter(request *http.Request, responseW http.ResponseWriter) (*http.Request, http.ResponseWriter) {
	base.Logger.Info().Printf(" from %s to %s \n", request.RemoteAddr, request.RequestURI)
	return request, responseW
}
