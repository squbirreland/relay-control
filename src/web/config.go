package web

import (
	"relay-control/src/machine/mvc"
)

func InitController() {
	mvc.QuestMapping[mvc.NewReq("GET", "/")] = MainPage
	mvc.QuestMapping[mvc.NewReq("GET", "/on")] = SendOn
	mvc.QuestMapping[mvc.NewReq("GET", "/off")] = SendOff
	mvc.Interceptors = append(mvc.Interceptors, inter)
}
