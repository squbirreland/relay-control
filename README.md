# relay-control

### 初始化流程

#### 1. 扫描配置并存储

    注意外化配置 config.json
    路径在当前jar包目录下
    其配置为:
    只有当前配置了的端口会被监听

#### 2. 初始化串口并配置属性与监听并结束初始化

### 运行流程

    ---

#### 自制MVC内部说明

1. 实现定义类型Controller或者Interceptor

```go
// Controller ...
type Controller func (request *http.Request, responseW http.ResponseWriter)

// Intercept ...
type Intercept func (request *http.Request, responseW http.ResponseWriter) (*http.Request, http.ResponseWriter)
```

2. 在init里增减映射
   ![img.png](resource/img.png)

## 使用说明

    设置编译环境
    SET CGO_ENABLED=0
    SET GOARCH=amd64

    SET GOOS=linux

    SET GOOS=windows
    编译
    go build -o ./target/ ./src/main.go

    linux 查看串口名 
    dmesg | grep ttyS*

    复制config.json到运行目录并配置

config.json属性说明

```json
{
  //串口读取灵敏度 单位纳秒 分别影响两次读取延迟
  //第一次为单次延迟 决定一条命令是否能读取完全 过低会被切割
  //第二次为间隔延迟 影响两条命令的最小间隔时间
  //该设置为单次延迟 间隔延迟默认为单次延迟的两倍
  "ReadSensitivity": 10,
  //串口配置
  "PortOptions": [
    {
      //串口名
      "PortName": "COM3",
      //比特率
      "BaudRate": 9600,
      //数据位
      "DataBits": 8,
      //停止位
      "StopBits": 1,
      //校验
      //ParityNone = 0
      //ParityOdd  = 1
      //ParityEven = 2
      "ParityMode": 0
      //其他配置有如下
      //InterCharacterTimeout uint 
      //MinimumReadSize       uint

      // -- Use to enable RS485 mode -- probably only valid on some Linux platforms
      //Rs485Enable bool
      // -- Set to true for logic level high during send
      //Rs485RtsHighDuringSend bool
      // -- Set to true for logic level high after send
      //Rs485RtsHighAfterSend bool
      // -- set to receive data during sending
      //Rs485RxDuringTx bool
      // -- RTS delay before send
      //Rs485DelayRtsBeforeSend int
      // -- RTS delay after send
      //Rs485DelayRtsAfterSend int 
    }
  ]
}
```

其余配置
    basic/constant中有其余配置 包括启动端口与监听等 可外化配置 但暂未实现
## 注意事项

    因为会直接发送命令到串口 所以消息载体数据类型需要为 byte[] 的16进制命令
    